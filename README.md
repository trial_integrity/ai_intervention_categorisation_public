# AI_intervention_categorisation_public

This is the public repository associated with the article "Evaluating the capacity of openly available Large Language Models in categorizing abstracts of Randomised Controlled Trials." by Diana Buitrago-Garcia, Delphine S. Courvoisier, Michele Iudici, and Denis Mongin.

The repository contains:

- the file [gold_standard.csv](./gold_standard.csv) with the RCT's abstract in the column `abstract_all` for each article with the pubmedid in the column `ID`, with the two reviewer classification (`Intervention` and `Intervention2` columns, for the untrained and trained reviewer respectively) and the subsequent gold standard (`Decision` column)
- the python files to run the inferece with the AI models tested are in the folder [python_llm_files](./python_llm_files).
  - bart in the [classify_abstract_bart.py](./python_llm_files/classify_abstract_bart.py) file
  - debert in the [classify_abstract_debert.py](./python_llm_files/classify_abstract_debert.py) file
  - llama3 in the [classify_abstract_llama3.py](./python_llm_files/classify_abstract_llama3.py) file, with the associated prompts in the files [prompts_llama3.py](./python_llm_files/prompts_llama3.py)
- the results of the inference are in the folder [categorisation_results](./categorisation_results)
- the R file parsong the results, and producing the tables and figures: [analyse.R](analyse.R)

Python script were run on a machine with GPU: NVIDIA Titan X with 12 GB of VRam, in conjunction with 20G of RAM standard CPU, with GCC/11.3.0 OpenMPI/4.1.4 PyTorch/1.12.0-CUDA-11.7.0 torchvision/0.13.1-CUDA-11.7.0.
