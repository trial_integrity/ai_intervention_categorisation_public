print("loading libraries")
from transformers import AutoTokenizer
import pandas as pd
import transformers
import torch
import numpy as np
import os
import sys
import gc
import datetime

input = int(sys.argv[1])-1 # reading argument from slurm multijob
# from 0 to 3, as there are 4 conditions tested in the project

# load data
full_data = pd.read_csv("intervention_to_classify.csv", encoding='latin-1')
# create result dataframe
result = full_data.assign(result = "")
# names of the output file
filename = "./results/result_llama3_" + str(input) + ".csv"

# read already done
if os.path.exists(filename):
    done = pd.read_csv(filename)
    print(str(len(done))+" already done. Removing")
    full_data = full_data.loc[~full_data.ID.isin(done.ID)].reset_index(drop=True) # removing classified abstract

# load classifier
print("loading classifier")

# 8 bit quantified llama3 model
model = "alokabhishek/Meta-Llama-3-8B-Instruct-bnb-8bit"
# define pipeline
tokenizer = AutoTokenizer.from_pretrained(model)
pipeline = transformers.pipeline(
    "text-generation",
    model=model,
    torch_dtype=torch.float16,
    device_map="auto"
)

# open prompts from the prompt file
prompt_file = "prompts_llama3.py"
exec(open(prompt_file).read())

# load the one corresponding to the job
preprompt = promp_list[input]


prompt_format="""<|start_header_id|>user<|end_header_id|>
Q: "{document_context}"<|eot_id|><|start_header_id|>assistant<|end_header_id|>
A:
"""
# terminators
terminators = [
    pipeline.tokenizer.eos_token_id,
    pipeline.tokenizer.convert_tokens_to_ids("<|eot_id|>")
]

# function to flush memory
def flush():
  gc.collect()
  torch.cuda.empty_cache()
  torch.cuda.reset_peak_memory_stats()

# loop on all lines of file
for i in full_data.index: 
  plouf = datetime.datetime.now() 
  # log
  print(plouf.strftime("%Y-%m-%d %Hh-%Mm-%Ss") + " : " + str(i) + " in " + str(len(full_data)))
  # read abstract
  sequence_to_classify = full_data.loc[i,"abstract_all"]
  # create prompt with abstract
  entire_prompt = preprompt + prompt_format.format(document_context=sequence_to_classify)
  # inference
  sequences = pipeline(
        entire_prompt,
        do_sample=True,
        top_k=40,
        top_p=0.95,
        repetition_penalty = 1,
        temperature = 0.1,
        num_return_sequences = 1,
        eos_token_id = terminators,
        max_new_tokens = 200,
    )
  # save result
  result.loc[result.index[i] ,"result"] =  sequences[0]['generated_text'].replace(entire_prompt,"")
  plouf = result.loc[[i]]
  plouf.to_csv(filename, mode='a' , header=not os.path.exists(filename))
  flush()
