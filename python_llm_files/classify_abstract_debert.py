print("loading libraries")
import pandas as pd
from transformers import pipeline
import numpy as np
# multi jobs
import sys
import os
import datetime


input = int(sys.argv[1])-1 # reading argument from slurm multijob
# from 0 to 6, as there are 6 set of category labels tested

# labels
candidate_list =  [["drug intervention","non-drug intervention"],
                   ["drug intervention","other"],
                   ["pharmacological treatment","non-pharmacological treatment"],
                   ["pharmacological treatment","other"],
                   ["drug","non-drug"],
                   ["drug intervention","Behavioral intervention","Health Care delivery change","wellness intervention",
                    "Device test","Educational intervention","physiotherapeutic intervention","Food and plant treatment","Lifestyle modification",
                    "Surgical intervention","Procedure","biological treatments"],
                   ["drug","Behavioral","Health Care delivery","wellness",
                    "Device","Educational","physiotherapeutic","Food and plant","Lifestyle",
                    "Surgical","Procedure","biological treatments"]]

# select the one for the job
candidate_labels = candidate_list[input]

print("loading data")
# load data
full_data = pd.read_csv("intervention_LLM.csv", encoding='latin-1')

# output file
filename = "./results/result_intervention_bart_" + str(input) + ".csv"
# read already done
if os.path.exists(filename):
    done = pd.read_csv(filename)
    print(str(len(done))+" already done. Removing")
    full_data = full_data.loc[~full_data.ID.isin(done.ID)].reset_index(drop=True)

# create result dataframe
result = full_data.assign(label ="",score = "")

# load classifier
print("loading classifier")
classifier = pipeline("zero-shot-classification", model="MoritzLaurer/DeBERTa-v3-base-mnli-fever-anli")

# loop on all lines of data frame
for i in full_data.index: 
  # log
  plouf = datetime.datetime.now() 
  print(plouf.strftime("%Y-%m-%d %Hh-%Mm-%Ss") + " : " + str(i) + " in " + str(len(full_data)))

  sequence_to_classify = full_data.loc[i,"abstract_all"]
  # inference
  output = classifier(sequence_to_classify, candidate_labels, multi_label=False)
  # save result
  result.loc[[i], 'score']  = pd.Series([output["scores"]], index=result.index[[i]])
  result.loc[[i], 'label']  = pd.Series([output["labels"]], index=result.index[[i]])
  plouf = result.loc[[i]]
  plouf.to_csv(filename, mode='a' , header=not os.path.exists(filename))

